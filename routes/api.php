<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('register', 'Auth\RegisterController');
Route::post('login', 'Auth\LoginController');
Route::post('logout', 'Auth\LogoutController');

Route::namespace('Article')->middleware('auth:api')->group(function () {
    Route::post('create-new-article', 'ArticleController@store');
    Route::patch('update-the-selected-article/{article}', 'ArticleController@update');
    Route::delete('delete-the-selected-article/{article}', 'ArticleController@destroy');
});

Route::get('articles/{article}', 'Article\ArticleController@show');
Route::get('articles', 'Article\ArticleController@index');

Route::get('user', 'UserController');
